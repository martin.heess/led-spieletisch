package de.dhbw.ledgametable.Animation;

import com.diozero.ws281xj.PixelAnimations;
import com.diozero.ws281xj.rpiws281x.WS281x;
import de.dhbw.ledgametable.IRTouch.TouchListener;
import de.dhbw.ledgametable.Main.Main;
import de.dhbw.ledgametable.Mode.Mode;

import java.util.ArrayList;

public class Animation implements TouchListener {

    private WS281x led_driver;
    private int status;
    private boolean repeat = true;

    public Animation(){

    }

    public void start(){
        status = 1;
        led_driver = new WS281x(10, 255, 12*22);
        while (repeat){
            switch (status){
                case 0:
                    PixelAnimations.rainbow(led_driver,50);
                    break;
                case 1:
                    PixelAnimations.rainbowCycle(led_driver,50);
                    break;
                case 2:
                    PixelAnimations.theatreChaseRainbow(led_driver,50);
                    break;
                case 3:
                    PixelAnimations.colourWipe(led_driver,32,50);
                    break;
                default:
                    PixelAnimations.demo(led_driver);
            }
        }
    }



    public void stop(){
        led_driver.close();
    }


    @Override
    public void onTouchEvent(ArrayList<Integer> touchedFields) {
        for(int next  : touchedFields){
            switch (next) {
                case 1:
                    status = 0;
                    break;
                case 2:
                    status = 1;
                    break;
                case 3:
                    status = 2;
                    break;
                case 4:
                    status = 3;
                    break;
                case 10:
                    Main.currentMode = Mode.MENU;
                    Main.status = false;
                    break;
            }
        }
    }
}
