package de.dhbw.ledgametable.Menu;

import com.diozero.ws281xj.rpiws281x.WS281x;
import de.dhbw.ledgametable.IRTouch.TouchListener;
import de.dhbw.ledgametable.Main.Main;
import de.dhbw.ledgametable.Mode.Mode;

import java.util.ArrayList;

public class Menu implements TouchListener {

    private WS281x led_driver;

    public Menu(){

    }

    public void start(){
        led_driver = new WS281x(10, 255, 12*22);
        printMenu();
    }

    private void printMenu() {


    }

    public void stop(){
        led_driver.close();
    }


    @Override
    public void onTouchEvent(ArrayList<Integer> touchedFields) {
        for(int next  : touchedFields){
            switch (next) {
                case 45:
                    Main.currentMode = Mode.TETRIS;
                    Main.status = false;
                    break;
                case 150:
                    Main.currentMode = Mode.ANIMATION;
                    Main.status = false;
                    break;
                case 234:
                    Main.currentMode = Mode.MENU;
                    Main.status = false;
                    break;
            }
        }
    }


}
