package de.dhbw.ledgametable.Tetris;

import com.diozero.ws281xj.PixelAnimations;
import com.diozero.ws281xj.PixelColour;
import com.diozero.ws281xj.rpiws281x.WS281x;
import de.dhbw.ledgametable.IRTouch.TouchDevice;
import de.dhbw.ledgametable.IRTouch.TouchListener;
import de.dhbw.ledgametable.utils.Time;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class TetrisBoard implements TouchListener {

    /*

                Tetrisfeld Struktur
    1   [  1,  2,  3,  4,  5,  6,  7,  8,  9, 10]
    2   [ 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
    3   [ 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
    4   [ 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]
    5   [ 41, 42, 43, 44, 45, 46, 47, 48, 49, 50]
    6   [ 51, 52, 53, 54, 55, 56, 57, 58, 59, 60]
    7   [ 61, 62, 63, 64, 65, 66, 67, 68, 69, 70]
    8   [ 71, 72, 73, 74, 75, 76, 77, 78, 79, 80]
    9   [ 81, 82, 83, 84, 85, 86, 87, 88, 89, 90]
    10  [ 91, 92, 93, 94, 95, 96, 97, 98, 99, 00]
    11  [101,102,103,104,105,106,107,108,109,110]
    12  [111,112,113,114,115,116,117,118,119,120]
    13  [121,122,123,124,125,126,127,128,129,130]
    14  [131,132,133,134,135,136,137,138,139,140]
    15  [141,142,143,144,145,146,147,148,149,150]
    16  [151,152,153,154,155,156,157,158,159,160]
    17  [161,162,163,164,165,166,167,168,169,170]
    18  [171,172,173,174,175,176,177,178,179,180]
    19  [181,182,183,184,185,186,187,188,189,190]
    20  [191,192,193,194,195,196,197,198,199,200]

     */

    private WS281x led_driver;

    private HashMap<Integer, Integer> field = new HashMap<>();
    private Block currentBlock;
    private int nextBlock;

    private int level = 0;
    private int rows = 0;
    private int score = 0;

    private Random r = new Random();

    public TetrisBoard() {

    }

    //Timer, der den Drop der Blöcke regelt.
    private Timer timer1 = new Timer(1500, e -> dropBlock());
    //Timer der das Spielfeld immer wieder updated.
    private Timer timer2 = new Timer(1, e -> paintField());

    //Startet die Timer und fügt den ersten Block ein.
    public void start() {

        System.out.println("Tetris Startet");
        led_driver = new WS281x(10, 255, 12 * 22);
        paintField();
        addNewBlock();
        timer1.start();
        timer2.start();

    }

    public void stop(){
        timer1.stop();
        timer2.stop();
        led_driver.close();
    }

    //Fügt einen neuen Block hinzu wenn der davorige am Boden angekommen ist.
    private void addNewBlock(){
        if(nextBlock == 0){
            nextBlock = r.nextInt(7) + 1;
        }
        currentBlock = new Block(nextBlock);
        nextBlock = r.nextInt(7) + 1;
        int[] tmp = new int[4];
        System.arraycopy(currentBlock.getType().getStartPlace(), 0, tmp, 0, 4);
        currentBlock.setCoordinates(tmp);
        if(!isPlaceble()) {
            timer1.stop();
            timer2.stop();
            paintField();
        }
    }

    //Lässt den Block pro Sekunde immer um ein Block nah untern fallen.
    private void dropBlock(){
        if(isOnFloor()) {
            int[] coordinates = currentBlock.getCoordinates();
            for (int i = 0 ; i < 4 ; i++){
                coordinates[i] = coordinates[i] + 10;
            }
            currentBlock.setCoordinates(coordinates);
        }
    }

    //Verschiebt den Block um eins nach Links.
    private void shiftLeft(){
        if(!isAtLeft()) {
            int[] coordinates = currentBlock.getCoordinates();
            for (int i = 0; i < 4; i++) {
                coordinates[i]--;
            }
            currentBlock.setCoordinates(coordinates);
        }
    }

    //Verschiebt den Block um eins nach Rechts.
    private void shiftRight(){
        if(!isAtRight()) {
            int[] coordinates = currentBlock.getCoordinates();
            for (int i = 0; i < 4; i++) {
                coordinates[i]++;
            }
            currentBlock.setCoordinates(coordinates);
        }
    }

    //Dreht den Block um 90°.
    private void turn(){
        int[] coordinates = new int[4];
        for (int i = 0; i < 4; i++) {
            coordinates[i] = currentBlock.getCoordinates()[i];
        }
        if(currentBlock.getStauts() == 3) {
            int[] turn = currentBlock.getType().getTurn()[0];
            for (int i = 0; i < 4; i++) {
                coordinates[i] = currentBlock.getCoordinates()[i] + turn[i];
            }
        } else {
            int[] turn = currentBlock.getType().getTurn()[currentBlock.getStauts() + 1];
            for (int i = 0; i < 4; i++) {
                coordinates[i] = currentBlock.getCoordinates()[i] + turn[i];
            }
        }
        if(isTurnable(coordinates)) {
            currentBlock.setCoordinates(coordinates);
            if(currentBlock.getStauts() == 3) {
                currentBlock.setStauts(0);
            } else {
                currentBlock.setStauts(currentBlock.getStauts() + 1);
            }
        }
    }

    //Verschiebt den aktuellen Block umd einen Block nach unten.
    private void shiftDown(){
        if(isOnFloor()) {
            int[] coordinates = currentBlock.getCoordinates();
            for (int i = 0; i < 4; i++) {
                coordinates[i] = coordinates[i] + 10;
            }
            currentBlock.setCoordinates(coordinates);
        }
    }

    //Prüft, ob ein neuer Block plaziert werden kann und ob das Feld schon belegt ist.
    private boolean isPlaceble(){
        boolean placeFlag = true;
        for(int i = 1 ; i < 4 ; i++){
            if (field.containsKey(currentBlock.getType().getStartPlace()[i])) {
                placeFlag = false;
                break;
            }
        }
        return placeFlag;
    }

    //Prüft, ob der aktuelle Block gedreht werden kann.
    private boolean isTurnable(int[] movement){
        int[] leftBoarder = {1,11,21,31,41,51,61,71,81,91,101,111,
                121,131,141,151,161,171,181,191};
        int[] rightBoarder = {10,20,30,40,50,60,70,80,90,100,110,
                120,130,140,150,160,170,180,190,200};
        boolean left = false;
        boolean right = false;
        boolean collective;
        for(int i = 0 ; i < 20 ; i++ ){
            for(int j = 0 ; j < 4 ; j++){
                if(leftBoarder[i] == movement[j]){
                    left = true;
                }
                if(rightBoarder[i] == movement[j]){
                    right = true;
                }
            }
        }
        collective = !(left && right);
        for(int i = 1 ; i < 4 ; i++)
            if (field.containsKey(movement[i])) {
                collective = false;
                break;
            }
        return collective;
    }

    //Prüft, ob der aktuelle Block am Boden angekommen ist.
    private boolean isOnFloor(){
        boolean onFloor = false;
        for(int i = 0 ; i < 4 ; i++) {
            if(currentBlock.getCoordinates() != null) {
                if ((currentBlock.getCoordinates()[i] + 10) > 200 ||
                        field.containsKey(currentBlock.getCoordinates()[i] + 10)) {
                    onFloor = true;
                }
            }
        }
        if(onFloor){
            fixCurrentBlock();
        }
        return !onFloor;
    }

    //Prüft, ob der Block am linken Rand ist und darüber nicht hinaus geht.
    private boolean isAtLeft(){
        int[] boarder = {1,11,21,31,41,51,61,71,81,91,101,111,
                121,131,141,151,161,171,181,191};
        boolean atLeft = false;
        for(int i = 0 ; i < 4 ; i++) {
            if (field.containsKey(currentBlock.getCoordinates()[i] - 1)) {
                atLeft = true;
            }
            for(int j = 0 ; j < 20 ; j++){
                if(currentBlock.getCoordinates()[i] == boarder[j]){
                    atLeft = true;
                }
            }
        }
        return atLeft;
    }

    //Prüft, ob der Block am rechten Rand ist und darüber nicht hinaus geht.
    private boolean isAtRight(){
        int[] boarder = {10,20,30,40,50,60,70,80,90,100,110,120,
                130,140,150,160,170,180,190,200};
        boolean atRight = false;
        for(int i = 0 ; i < 4 ; i++) {
            if (field.containsKey(currentBlock.getCoordinates()[i] + 1)) {
                atRight = true;
            }
            for(int j = 0 ; j < 20 ; j++){
                if(currentBlock.getCoordinates()[i] == boarder[j]){
                    atRight = true;
                }
            }
        }
        return atRight;
    }

    //Fixiert den aktuellen Block im Feld wenn er unten angekommen ist.
    private void fixCurrentBlock(){
        for(int i = 0 ; i < 4 ; i++) {
            field.put(currentBlock.getCoordinates()[i], currentBlock.getType().getColor());
        }
        if(checkRow()){
            deleteAndCountRow();
        }
        addNewBlock();
    }

    //Überprüft, ob eine Reihe komplett gefüllt ist.
    private boolean checkRow(){
        boolean sum = false;
        for(int i = 0 ; i < 20 ; i++){
            int v = 0;
            for(int j = 0 ; j < 10 ; j++){
                int number = (i * 10) + j + 1;
                if(field.containsKey(number)){
                    v++;
                }
            }
            if(v == 10){
                return true;
            }
        }
        return false;
    }

    //Zählt die kompletten Reihen und verschiebt die restlichen Blöcke nach unten.
    private void deleteAndCountRow(){
        int count = 0;
        for(int i = 0 ; i < 20 ; i++){
            int v = 0;
            for(int j = 0 ; j < 10 ; j++){
                int number = (i * 10) + j + 1;
                if(field.containsKey(number)){
                    v++;
                }
                if(v == 10){
                    int first = number - 9;
                    for(int f = first ; f < first + 10 ; f++){
                        field.remove(f);
                    }
                    for(int g = 200 ; g > 0; g--){
                        if(field.containsKey(g)){
                            if(g<first){
                                int tmp = field.get(g);
                                field.remove(g);
                                field.put(g + 10,tmp);
                        }
                        }
                    }
                    count++;
                }
            }
        }
        score = score + ((10*100) * count);
        rows = rows + count;
        checkLevel();
    }

    //Setzt das Level nach dem Menge der abgearbeiteten Reihen.
    private void checkLevel(){
        int tmp = rows % 10;
        level = Math.min(((rows - tmp) / 10), 10);
        timer1.setDelay(1500-(100*level));
        timer1.restart();
    }

    //Generiert das Spielfeld.
    private void paintField(){

        int[] startStrip = {221,218,177,174,133,130,89,86,45,42};
        int flag = 1;

        if(timer1.isRunning() && timer2.isRunning()) {
            for(int i = 0 ; i < 20; i++){
                for(int j = 0; j < 10; j++){
                    if(j % 2 == 0){
                        led_driver.setPixelColour(startStrip[j]+i,0);
                        if(field.containsKey(flag)) {
                            led_driver.setPixelColour(startStrip[j]+i, field.get(flag));
                        }
                        for (int k = 0; k < 4; k++) {
                            if (currentBlock.getCoordinates() != null) {
                                if (currentBlock.getCoordinates()[k] == flag) {
                                    led_driver.setPixelColour(startStrip[j]+i,
                                            currentBlock.getType().getColor());
                                }
                            }
                        }
                        flag++;
                    } else {
                        led_driver.setPixelColour(startStrip[j]-i,0);
                        if(field.containsKey(flag)) {
                            led_driver.setPixelColour(startStrip[j]-i, field.get(flag));
                        }
                        for (int k = 0; k < 4; k++) {
                            if (currentBlock.getCoordinates() != null) {
                                if (currentBlock.getCoordinates()[k] == flag) {
                                    led_driver.setPixelColour(startStrip[j]-i,
                                            currentBlock.getType().getColor());
                                }
                            }
                        }
                        flag++;
                    }
                }
            }

            paintFrame();

        }
        led_driver.render();
        Time.delay(50);
    }

    private void paintFrame(){
        int[] top = {0,263,220,219,176,175,132,131,88,87,44,43};
        int[] bot = {242,241,198,197,154,153,110,109,66,65,22,21};
        for(int i = 1 ; i < 21; i++){
            led_driver.setPixelColour(top[0] + i, PixelColour.createColourRGB(255,255,255));
            led_driver.setPixelColour(bot[0] + i, PixelColour.createColourRGB(255,255,255));
        }
        for(int i = 0; i < 12; i++){
            led_driver.setPixelColour(top[i], PixelColour.createColourRGB(255,255,255));
            led_driver.setPixelColour(bot[i], PixelColour.createColourRGB(255,255,255));
        }
    }

    @Override
    public void onTouchEvent(ArrayList<Integer> touchedFields) {
        if(timer1.isRunning() && timer2.isRunning()) {
            for(int next  : touchedFields){
                switch (next) {
                    case 137:
                        turn();
                        break;
                    case 246:
                        shiftDown();
                        break;
                    case 13:
                        shiftLeft();
                        break;
                    case 9:
                        shiftRight();
                        break;
                }
            }
        }
    }
}
