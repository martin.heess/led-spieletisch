package de.dhbw.ledgametable.Tetris;

public class Block {

    private int[] coordinates;

    private BlockType type;

    private int stauts;

    public Block(int id) {
        type = BlockType.getBlockById(id);
        stauts = 0;
    }

    public BlockType getType() {
        return type;
    }

    public int[] getCoordinates(){
        return coordinates;
    }

    public int getStauts() {
        return stauts;
    }

    public void setCoordinates(int[] coordinates) {
        this.coordinates = coordinates;
    }

    public void setStauts(int stauts) {
        this.stauts = stauts;
    }

}


