package de.dhbw.ledgametable.Tetris;

import com.diozero.ws281xj.PixelColour;

public enum BlockType {

    I (1, PixelColour.LIGHT_BLUE, new int[] {4,5,6,7},
            new int[][] {{9,0,-9,-18},{-9,0,9,18},{9,0,-9,-18},{-9,0,9,18}}),
    T (2, PixelColour.YELLOW, new int[] {4,5,6,15},
            new int[][] {{9,0,0,0},{-9,-1,-1,0},{0,0,0,-9},{0,1,1,9}}),
    L (3, PixelColour.GREEN, new int[] {4,5,6,14},
            new int[][] {{9,0,-9,-2},{-10,-10,-1,1},{2,9,0,-9},{-1,1,10,10}}),
    J (4, PixelColour.BLUE, new int[] {4,5,6,16},
            new int[][] {{9,9,1,1},{-9,0,9,-2},{-1,-1,-10,-8},{1,-8,0,9}}),
    Z (5, PixelColour.ORANGE, new int[] {4,5,15,16},
            new int[][] {{8,0,9,1},{-8,0,-9,-1},{8,0,9,1},{-8,0,-9,-1}}),
    N (6, PixelColour.PURPLE, new int[] {14,15,5,6},
            new int[][] {{9,0,-11,-20},{-9,0,11,20},{9,0,-11,-20},{-9,0,11,20}}),
    O (7, PixelColour.RED, new int[] {5, 6, 15, 16},
            new int[][] {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}});

    private int  id;
    private int color;
    private int[] startPlace;
    private int[][] turn;

    BlockType(int id, int color, int[] startPlace, int[][] turn) {
        this.id = id;
        this.color = color;
        this.startPlace = startPlace;
        this.turn = turn;
    }

    public static BlockType getBlockById(int id){
        switch (id){
            case 1:
                return I;
            case 2:
                return T;
            case 3:
                return L;
            case 4:
                return J;
            case 5:
                return Z;
            case 6:
                return N;
            case 7:
                return O;
            default:
                return null;
        }
    }

    public int getColor() {
        return color;
    }

    public int[] getStartPlace() {
        return startPlace;
    }

    public int[][] getTurn() {
        return turn;
    }
}
