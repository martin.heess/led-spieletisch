package de.dhbw.ledgametable.IRTouch;

import org.firmata4j.IODevice;
import org.firmata4j.Pin;
import org.firmata4j.firmata.FirmataDevice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class TouchDevice extends Thread {

    private IODevice device = new FirmataDevice("/dev/ttyUSB0");
    private int[] digitalPinOut = {23,24,25,26,27,28,29,30,31,32,22,33,34,35,36,37,38,39,40,41,42,43};
    private Pin[] digitalPins = new Pin[22];
    private Pin[] analogPins = new Pin[12];
    private HashMap<Integer, Long> touchFaktor = new HashMap<>();
    private HashMap<Integer, Long> tmpFaktor = new HashMap<>();
    private HashMap<Integer, Long> touchMatrix = new HashMap<>();

    private TouchListener touchListener;

    public TouchDevice() {
    }

    @Override
    public void run(){
        try {
            init();
            start();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void init() throws IOException, InterruptedException {
        //Start the device
        device.start();
        while(!device.isReady()){ };
        //Init the digitalPins for the controll of the infrared leds
        int flag = 0;
        for(int i = 22; i > 0; i--){
            Pin pin = device.getPin(digitalPinOut[i - 1]);
            pin.setMode(Pin.Mode.OUTPUT);
            pin.setValue(0);
            digitalPins[flag] = pin;
            flag++;
        }
        //Init the analog pins for the voltage drop
        for(int i = 0; i < 12; i++){
            Pin pin = device.getPin(54 + i);
            pin.setMode(Pin.Mode.ANALOG);
            analogPins[i] = pin;
        }

        scanBoard(touchFaktor);
        for(int i = 0; i < 5 ; i++){
            tmpFaktor = touchFaktor;
            scanBoard(touchFaktor);
            for(int j = 1; j < touchFaktor.size() ; j++){
                touchFaktor.replace(j,(touchFaktor.get(j) + tmpFaktor.get(j))/2);
            }
        }
        /*
        for(int i = 1 ; i < touchFaktor.size() + 1 ; i++){
            System.out.print("| " + touchFaktor.get(i));
            if(i % 12 == 0){
                System.out.println("\n");
            }
        }

         */

        TimeUnit.SECONDS.sleep(5);

    }

    public void start() {
        System.out.println("Initial IR-Touchscreen");
        try {
            init();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        while (true) {
                System.out.println("Scan");
                scanBoard(touchMatrix);
                checkTouch();
                /*
                for (int i = 1; i < touchMatrix.size() + 1; i++) {
                    double tmp = (double) touchMatrix.get(i) / (double) touchFaktor.get(i);
                    if (tmp < 0.6) {
                        System.out.print("| TRUE");
                    } else {
                        System.out.print("| FALS");
                    }
                    if (i % 12 == 0) {
                        System.out.println("\n");
                    }
                }

                 */
            }
    }

    private void scanBoard(HashMap<Integer, Long> map){

        try {
            for(int i = 0; i < 22; i++){
                //Light up the LED-row
                digitalPins[i].setValue(1);
                TimeUnit.MILLISECONDS.sleep(30);
                for(int j = 0; j < 12 ; j++){
                    int field = i * 12 + j;
                    long value = analogPins[j].getValue();
                    map.put(field+1, value);
                }
                digitalPins[i].setValue(0);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void checkTouch() {
        ArrayList<Integer> touchedFields = new ArrayList<>();
        for (int i = 0; i < 22; i++) {
            for (int j = 0; j < 12; j++) {
                double tmp = (double) touchMatrix.get(i * 12 + j + 1)
                        / (double) touchFaktor.get(i * 12 + j + 1);
                if (tmp < 0.6) {
                    touchedFields.add(i * 12 + j + 1);
                }
            }
        }
        if (!touchedFields.isEmpty()) {
            System.out.println(touchedFields.toString());
            touchListener.onTouchEvent(touchedFields);
        }
    }
}
