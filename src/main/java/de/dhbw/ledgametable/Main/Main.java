package de.dhbw.ledgametable.Main;

import de.dhbw.ledgametable.Animation.Animation;
import de.dhbw.ledgametable.IRTouch.TouchDevice;
import de.dhbw.ledgametable.Menu.Menu;
import de.dhbw.ledgametable.Mode.Mode;
import de.dhbw.ledgametable.Tetris.TetrisBoard;

import java.io.IOException;


public class Main{


    /*public static void main(String[] args){

        //Initiallisierung des Ledstreifens mit dem Augang auf Pin 10
        WS281x led_stip = new WS281x(10, 255, 12*22);

        //Testen von einzelnen Leds
        led_stip.allOff();
        for(int i = 0 ; i < 12 * 22 ; i++){
            led_stip.setPixelColour(i, PixelColour.BLUE);
            led_stip.render();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        led_stip.allOff();

        //Abspielen der Demonstration
        PixelAnimations.demo(led_stip);

        led_stip.allOff();
    }


     */

/*
    public static void main(String[] args){

        System.out.println("Test der Firmata-Bibliothek und der Arduino Programmierung");

        //Implementierung des Arduino Mega, welcher über USB verbunden ist.
        IODevice device = new FirmataDevice("/dev/ttyUSB0");

        try {
            device.start();
            while (!device.isReady());

            Pin led = device.getPin(32);
            led.setMode(Pin.Mode.OUTPUT);

            Pin input = device.getPin(12);
            input.setMode(Pin.Mode.ANALOG);

            System.out.println("Testet die digitalen Ausgänge der GPIO-Pins");
            for(int i = 0 ; i < 5 ; i++){
                System.out.println("Led an!");
                led.setValue(1);
                Thread.sleep(500);
                System.out.println("Led aus!");
                led.setValue(0);
                Thread.sleep(500);
            }

            System.out.println("Testen der analogen Pins");
            for(int i = 0 ; i < 5 ; i++){
                System.out.println("Wert des analogen Pins: " + input.getValue());
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            System.out.println("Es kam zu Fehlern bei der Verwendung mit dem Firmata-Protokoll!");
        }

    }


 */
    public Thread touchThread;

    public static Mode currentMode;
    public Mode preMode;
    public static boolean status = true;

    public Main() throws IOException, InterruptedException {
        //Init IR-Touch
        touchThread = new TouchDevice();
        touchThread.start();

        //Init the Mode
        currentMode = Mode.MENU;
        preMode = currentMode;

        Menu menu = null;
        TetrisBoard tetrisBoard = null;
        Animation animation = null;

        while (true) {
            if(status) {
                status = false;
                try {
                    switch (preMode) {
                        case MENU:
                            assert menu != null;
                            menu.stop();
                            break;
                        case GAME:
                            assert tetrisBoard != null;
                            tetrisBoard.stop();
                            break;
                        case ANIMATION:
                            assert animation != null;
                            animation.stop();
                            break;
                        default:
                    }
                    switch (currentMode) {
                        case MENU:
                            menu = new Menu();
                            menu.start();
                            break;
                        case GAME:
                            tetrisBoard = new TetrisBoard();
                            tetrisBoard.start();
                            break;
                        case ANIMATION:
                            animation = new Animation();
                            animation.start();
                            break;
                        default:
                    }
                    preMode = currentMode;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    }

   public static void main(String[] args) throws IOException, InterruptedException {
        new Main();
    }


}
